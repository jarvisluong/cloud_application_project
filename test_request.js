const http = require("http");

const getRequest = http.request(
  {
    host: "127.0.0.1",
    port: 8001,
    path: "/",
    method: "GET"
  },
  function incomingMessage(incomingMessageRes) {
    incomingMessageRes.setEncoding("utf8");
    incomingMessageRes.on("data", function handleIncomingData(chunk) {
      console.log(chunk);
    });
  }
);
getRequest.end();
