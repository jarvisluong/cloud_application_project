### Cloud Application Exercise

#### To run the stack

* First, you need to build an image for the service2 first:

```bash
docker build -t service2 -f ./service2/Dockerfile ./service2
```

* Then you can run

```bash
docker-compose up --build
```

* To terminate the stack, run

```bash
docker-compose down
```