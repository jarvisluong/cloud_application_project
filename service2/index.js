const http = require("http");

http
  .createServer(function listener(req, res) {
    if (req.method === "GET") {
        let responseContent = `Hello from ${req.client.remoteAddress}:${req.client.remotePort} to ${req.client.localAddress}:${req.client.localPort}`;
        res.write(responseContent);
        res.end();
      } else if (req.method === 'POST') {
        // Shutdown the service
        res.write('Bye bye!');
        res.end(() => {
          process.exit();
        });
      } else {

        res.writeHead(405, "Method is not allowed");
        res.end();
      }
  })
  .listen(3000);
