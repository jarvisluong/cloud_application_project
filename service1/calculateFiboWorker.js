const { workerData, parentPort } = require("worker_threads");
const calculateFibo = require("./fibo/calculateFibo");

try {
  const result = calculateFibo(workerData.number);
  parentPort.postMessage({ result, success: true });
} catch (error) {
  parentPort.postMessage({ result: error.message, success: false });
}
