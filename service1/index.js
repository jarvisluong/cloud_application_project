const express = require("express");
const axios = require("axios");
const fs = require("fs");
const uuid = require('uuid/v4');
const { Worker } = require("worker_threads");

const logPath = "/run-log/app.log";
const service2Endpoint = `http://${process.env.SERVICE2_HOST}:3000`;

const app = express();
app.use(express.urlencoded());
const port = 8001;

const fiboWorkers = {};

app.get("/", (req, res) => {
  axios
    .get(service2Endpoint)
    .then(({ data }) => {
      res
        .status(200)
        .send(
          `Hello from ${req.connection.remoteAddress}:${req.connection.remotePort} to ${req.connection.localAddress}:${req.connection.localPort}. ${data}`
        );
    })
    .catch(err => {
      console.error(err);
      res.status(500).send("Something went wrong!");
    });
});

app.get("/fibo", (req, res) => {
  const number = parseInt(req.body.number, 10);
  const workerId = uuid();
  const worker = new Worker("./calculateFiboWorker.js", { workerData: {number} });
  fiboWorkers[workerId] = worker;
  worker.on("message", data => {
    if (data.success) {
      res.send({result: data.result});
    } else {
      res.status(400).send(`Error: ${data.result}`);
    }
    fiboWorkers[workerId] = undefined;
  });
  worker.on("error", err => {
    res.status(400).send(`Error: ${err.message}`);
    fiboWorkers[workerId] = undefined;
  });
  worker.on("exit", exitCode => {
    if (exitCode > 0) {
      res.status(499).send('Calculation is cancelled');
    }
  })
});

app.post('/stop', (req, res) => {
  Object.values(fiboWorkers).forEach(worker => worker.terminate());
  res.send('OK');
})

app.get("/run-log", (req, res) => {
  fs.readFile(logPath, (err, data) => {
    if (err) {
      res.send("The log is empty");
    } else {
      res.send(data.toString("utf8"));
    }
  });
});

app.post("/shutdown", (req, res) => {
  axios
    .post(service2Endpoint)
    .then(() => {
      res.send("Bye bye!");
      fs.appendFile(logPath, `SHUTDOWN ${new Date().toISOString()}\n`, () => {
        process.exit(0);
      });
    })
    .catch(err => {
      console.error(err);
      res.send("Something went wrong, please try again!");
    });
});

app.listen(port, () => {
  fs.exists(logPath, isExist => {
    const bootString = `BOOT ${new Date().toISOString()}\n`;
    if (!isExist) {
      fs.writeFile(logPath, bootString, () => null);
    } else {
      fs.appendFile(logPath, bootString, () => null);
    }
  });
  console.log(`The application is listening on port ${port}`);
});
