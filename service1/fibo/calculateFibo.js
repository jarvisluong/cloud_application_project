module.exports = function calculateFibo(param) {
    if (typeof param === 'number'&& !isNaN(param)) {
        if (param < 0) {
            throw new Error('negative number')
        }
        return fiboCalcRecursive(param)
    } else {
        throw new Error('not a number')
    }
}

function fiboCalcRecursive(x) {
    if (x === 0) return 0
    if (x === 1) return 1
    return fiboCalcRecursive(x-1) + fiboCalcRecursive(x-2)
}