const calculateFibo = require('../calculateFibo')

test('should calculate fibo correctly', () => {
    expect(calculateFibo(0)).toBe(0);
    expect(calculateFibo(1)).toBe(1);
    expect(calculateFibo(10)).toBe(55);
    expect(calculateFibo(20)).toBe(6765);
});

test('should throw an error with negative number', () => {
    expect(() => calculateFibo(-1)).toThrow('negative number')
})

test('should throw error not a number', () => {
    expect(() => calculateFibo('abc').toThrow('not a number'))
    expect(() => calculateFibo(NaN)).toThrow('not a number')
})